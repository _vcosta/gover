package main

import (
	"fmt"
	"log"
	"strconv"
	"strings"

	gitlab "github.com/xanzy/go-gitlab"
)

func main() {
	git, err := gitlab.NewClient("jQtbFgKxSsSU4oY9yATJ")
	if err != nil {
		log.Fatal(err)
	}

	repository := "d1m3/semver_ci"
	label := "major"

	tags, _, err := git.Tags.ListTags(repository, &gitlab.ListTagsOptions{})
	tag := tags[0]

	log.Printf("tags: %s", tag.Name)
	log.Printf("tags: %s", tag.Commit.ID)

	version := getVersion(tag.Name)

	switch label {
	case "major":
		version[0] = version[0] + 1
		version[1] = 0
		version[2] = 0
		log.Println(versionToTag(version, "."))
	case "minor":
		version[1] = version[1] + 1
		version[2] = 0
		log.Println(versionToTag(version, "."))
	case "patch":
		version[2] = version[2] + 1
		log.Println(versionToTag(version, "."))
	default:
		log.Printf("Tag not applied")
	}

}

func versionToTag(a []int, delim string) string {
	return strings.Trim(strings.Replace(fmt.Sprint(a), " ", delim, -1), "[]")
}

func getVersion(str string) []int {
	var versionInt = []int{}

	version := strings.Split(str, ".")

	for _, i := range version {
		j, err := strconv.Atoi(i)
		if err != nil {
			panic(err)
		}
		versionInt = append(versionInt, j)
	}

	if len(versionInt) < 3 {
		panic("Version do not follow the patter X.Y.Z")
	}
	return versionInt
}
